
require 'rpm/c'
require 'rpm/package'
require 'rpm/db'
require 'rpm/problem'
require 'rpm/transaction'
require 'rpm/match_iterator'
require 'rpm/version'
require 'rpm/dependency'
require 'rpm/utils'

module XRPM
  TAG = XRPM::C::Tag
  LOG = XRPM::C::Log
  SENSE = XRPM::C::Sense
  FILE = XRPM::C::FileAttrs
  FILE_STATE = XRPM::C::FileState
  TRANS_FLAG = XRPM::C::TransFlags
  PROB_FILTER = XRPM::C::ProbFilter
  MIRE = XRPM::C::RegexpMode

  # Creates a new transaction and pass it
  # to the given block
  #
  # @param [String] root dir, default '/'
  #
  # @example
  #   XRPM.transaction do |ts|
  #      ...
  #   end
  #
  def self.transaction(root = '/')
    ts = Transaction.new
    ts.root_dir = root
    yield ts
  ensure
    ts.ptr.free
  end

  # @param [String] name Name of the macro
  # @return [String] value of macro +name+
  def self.[](name)
    val = ''
    buffer = ::FFI::MemoryPointer.new(:pointer, 1024)
    buffer.write_string("%{#{name}}")
    ret = XRPM::C.expandMacros(nil, nil, buffer, 1024)
    buffer.read_string
  end

  # Setup a macro
  # @param [String] name Name of the macro
  # @param [String] value Value of the macro or +nil+ to delete it
  def self.[]=(name, value)
    if value.nil?
      XRPM::C.delMacro(nil, name.to_s)
    else
      XRPM::C.addMacro(nil, name.to_s, '', value.to_s, XRPM::C::RMIL_DEFAULT)
    end
  end
end

XRPM::C.rpmReadConfigFiles(nil, nil)
XRPM::C.rpmInitMacros(nil, XRPM::C.MACROFILES)

# TODO
# set verbosity

require 'rpm/compat'
