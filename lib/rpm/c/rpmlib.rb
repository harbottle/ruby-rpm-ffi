
module XRPM
  module C
    attach_variable :XRPMVERSION, :RPMVERSION, :string
    attach_variable :XRPMEVR, :rpmEVR, :string

    attach_function 'rpmReadConfigFiles', [:string, :string], :int

    # ...
    attach_function 'rpmvercmp', [:string, :string], :int
  end
end
