require 'rpm'

module XRPM
  class Dependency
    # @return [String] dependency name
    attr_accessor :name
    # @return [String] dependency version
    attr_accessor :version
    # @return [String] dependency flags
    attr_accessor :flags
    # @return [Package] package this dependency belongs to
    attr_accessor :owner

    attr_accessor :nametag
    attr_accessor :versiontag
    attr_accessor :flagstag

    def initialize(name, version, flags, owner)
      XRPM::Utils.check_type(version, XRPM::Version)

      @name = name
      @version = version
      @flags = flags
      @owner = owner
    end

    # @param [Package, Dependency, Version] other
    # @return [Boolean] true if +other+ satisfies this dependency
    def satisfy?(other)
      case other
      when XRPM::Package then
        other.provides.each do |prov|
          return true if satisfy?(prov)
        end
        return false
      when XRPM::Dependency then
        XRPM::C.rpmdsCompare(
          XRPM::C.rpmdsSingle(:providename, other.name,
                             other.version.to_vre, other.flags),
          XRPM::C.rpmdsSingle(:providename, name,
                             version.to_vre, flags)
        ) != 0
      when XRPM::Version then
        XRPM::C.rpmdsCompare(
          XRPM::C.rpmdsSingle(:providename, name,
                             other.to_vre, other.to_vre.empty? ? 0 : :equal),
          XRPM::C.rpmdsSingle(:providename, name,
                             version.to_vre, flags)
        ) != 0
      else
        raise(TypeError, "#{other} is not a Version or Dependency")
      end
    end

    # @return [Boolean] true if '<' or '=<' are used to compare the version
    def lt?
      flags & XRPM::SENSE[:less]
    end

    # @return [Boolean] true if '>' or '>=' are used to compare the version
    def gt?
      flags & XRPM::SENSE[:greater]
    end

    # @return [Boolean] true if '=', '=<' or '>=' are used to compare the version
    def eq?
      flags & XRPM::SENSE[:equal]
    end

    # @return [Boolean] true if '=<' is used to compare the version
    def le?
      (flags & XRPM::SENSE[:less]) && (flags & XRPM::SENSE[:equal])
    end

    # @return [Boolean] true if '>=' is used to compare the version
    def ge?
      (flags & XRPM::SENSE[:greater]) && (flags & XRPM::SENSE[:equal])
    end

    # @return [Boolean] true if this is a pre-requires
    def pre?
      flags & XRPM::SENSE[:prereq]
    end
  end

  class Provide < Dependency
    def initialize(name, version, flags, owner)
      super(name, version, flags, owner)
      @nametag = XRPM::TAG[:providename]
      @versiontag = XRPM::TAG[:provideversion]
      @flagstag = XRPM::TAG[:provideflags]
    end
  end

  class Require < Dependency
    def initialize(name, version, flags, owner)
      super(name, version, flags, owner)
      @nametag = XRPM::TAG[:requirename]
      @versiontag = XRPM::TAG[:requireversion]
      @flagstag = XRPM::TAG[:requireflags]
    end
  end

  class Conflict < Dependency
    def initialize(name, version, flags, owner)
      super(name, version, flags, owner)
      @nametag = XRPM::TAG[:conflictname]
      @versiontag = XRPM::TAG[:conflictversion]
      @flagstag = XRPM::TAG[:conflictflags]
    end
  end

  class Obsolete < Dependency
    def initialize(name, version, flags, owner)
      super(name, version, flags, owner)
      @nametag = XRPM::TAG[:obsoletename]
      @versiontag = XRPM::TAG[:obsoleteversion]
      @flagstag = XRPM::TAG[:obsoleteflags]
    end
  end
end
