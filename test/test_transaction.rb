require_relative('helper')
require 'tmpdir'
require 'pathname'

class XRPMTransactionTests < MiniTest::Unit::TestCase
  PACKAGE_FILENAME = 'simple-1.0-0.i586.rpm'.freeze

  def test_flags
    XRPM.transaction do |t|
      assert_equal XRPM::TRANS_FLAG_NONE, t.flags
      t.flags = XRPM::TRANS_FLAG_TEST
      assert_equal XRPM::TRANS_FLAG_TEST, t.flags
    end
  end

  def test_iterator
    XRPM.transaction do |t|
      it = t.init_iterator(nil, nil)
      assert_kind_of XRPM::MatchIterator, it
      # assert it.count > 0
    end

    XRPM.transaction do |t|
      it = t.init_iterator(nil, nil)
      it.regexp(:name, :glob, '*audio*')
      it.each do |pkg|
        assert pkg.name.include?('audio'), "'#{pkg.name}' contains 'audio'"
      end
    end
  end

  # FIXME: this is not working
  def test_iterator_version
    XRPM.transaction do |t|
      it = t.init_iterator(nil, nil)
      it.version(XRPM::Version.new('2.1'))
      it.each do |sig|
        # FIXME: check that this worked
      end
    end
  end

  def test_basic_transaction_setters
    Dir.mktmpdir do |dir|
      XRPM.transaction do |t|
        assert_equal '/', t.root_dir
        t.root_dir = dir
        assert_equal dir + '/', t.root_dir
      end
    end

    Dir.mktmpdir do |dir|
      XRPM.transaction(dir) do |t|
        assert_equal dir + '/', t.root_dir
      end
    end
  end

  def test_test_flag_install
    filename = 'simple-1.0-0.i586.rpm'
    pkg = XRPM::Package.open(fixture(filename))

    Dir.mktmpdir do |dir|
      XRPM.transaction(dir) do |t|
        t.flags = XRPM::TRANS_FLAG_TEST
        t.install(pkg, fixture(filename))
        t.commit

        assert File.exist?(File.join(dir, XRPM['_dbpath'], 'Packages')),
               'rpm db exists'

        assert !File.exist?('/usr/share/simple/README'),
               "package #{pkg} was not installed"
      end
    end
  end

  def test_install_and_remove
    pkg = XRPM::Package.open(fixture(PACKAGE_FILENAME))

    Dir.mktmpdir do |dir|
      XRPM.transaction(dir) do |t|
        begin
          t.install(pkg, fixture(PACKAGE_FILENAME))
          t.commit

          assert File.exist?(File.join(dir, XRPM['_dbpath'], 'Packages')),
                 'rpm db exists'

          assert File.exist?(File.join(dir, '/usr/share/simple/README')),
                 "package #{pkg} should be installed"
        ensure
          # Force close so that XRPM does not try to do it
          # when the tmpdir is deleted
          t.db.close
        end
      end

      XRPM.transaction(dir) do |t|
        begin

          assert_raises TypeError do
            t.delete(Object.new)
          end

          t.delete(pkg)

          t.order
          t.clean

          t.commit

          assert !File.exist?(File.join(dir, '/usr/share/simple/README')),
                 "package #{pkg} should not be installed"

        ensure
          # Force close so that XRPM does not try to do it
          # when the tmpdir is deleted
          t.db.close
        end
      end
    end
  end

  def test_install_with_custom_callback
    pkg = XRPM::Package.open(fixture(PACKAGE_FILENAME))

    Dir.mktmpdir do |dir|
      XRPM.transaction(dir) do |t|
        begin
          t.install(pkg, fixture(PACKAGE_FILENAME))

          t.check do |problem|
            STDERR.puts "Problem: #{problem}"
          end

          t.order
          t.clean

          t.commit do |data|
            next case data.type
                 when :inst_open_file then
                   @f = File.open(data.key, 'r')
                 when :inst_close_file then @f.close
                 end
          end

          assert File.exist?(File.join(dir, '/usr/share/simple/README')),
                 "package #{pkg} should be installed"
        ensure
          # Force close so that XRPM does not try to do it
          # when the tmpdir is deleted
          t.db.close
        end
      end
    end
  end
end
