require File.join(File.dirname(__FILE__), 'helper')

class XRPMXRPMTests < MiniTest::Unit::TestCase
  def test_enum
    assert XRPM::TAG[:not_found]
  end

  def test_compat
    # puts XRPM::LOG_ALERT
    # assert_raise(NameError) { XRPM::LOG_ALERT }

    # require 'rpm/compat'
    # Nothing should be raised by the following statement
    XRPM::LOG_ALERT
    assert_equal XRPM::LOG_ALERT, XRPM::LOG[:alert]
  end

  def test_iterator
    XRPM.transaction do |t|
      assert_kind_of XRPM::Transaction, t
      # t.each do |pkg|
      #  puts pkg[:name]
      # end
    end
  end

  def test_macro_read
    assert_equal '/usr', XRPM['_usr']
  end

  def test_macro_write
    XRPM['hoge'] = 'hoge'
    assert_equal(XRPM['hoge'], 'hoge')
  end
end # class XRPM_XRPM_Tests < Test::Unit::TestCase
