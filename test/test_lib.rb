require File.join(File.dirname(__FILE__), 'helper')

class XRPMLibTests < MiniTest::Unit::TestCase
  def test_lib_lib
    assert_kind_of String, XRPM::C.XRPMVERSION
    # "x.y.z"
    assert(XRPM::C.XRPMVERSION.size >= 5)
    assert_kind_of Fixnum, XRPM::C.rpm_version_code
    # >= 4.0.0
    assert(XRPM::C.rpm_version_code >= ((4 << 16) + (0 << 8) + (0 << 0)))
  end

  def test_lib_header
    ptr = XRPM::C.headerNew
    XRPM::C.headerFree(ptr)
  end

  def test_lib_ts
    ts = XRPM::C.rpmtsCreate
    XRPM::C.rpmtsSetRootDir(ts, '/')
    it = XRPM::C.rpmtsInitIterator(ts, 0, nil, 0)
    hdrs = []
    until (hdr = XRPM::C.rpmdbNextIterator(it)).null?
      hdrs << hdr
      assert_kind_of String, XRPM::C.headerGetAsString(hdr, :name)
    end
    XRPM::C.rpmdbFreeIterator(it)
  end

  def test_lib_macros
    assert_kind_of String, XRPM::C.MACROFILES
  end
end
