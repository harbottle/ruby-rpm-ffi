require_relative('helper')
require 'rpm/compat'

class XRPMDependencyTests < MiniTest::Unit::TestCase
  EQ = XRPM::SENSE_EQUAL
  LT = XRPM::SENSE_LESS
  GT = XRPM::SENSE_GREATER

  def test_satisfy
    prv = provides('foo', '2', '1', 0, EQ)
    req = requires('foo', '1', '1', 0, EQ | GT)
    assert(req.satisfy?(prv))
    assert(prv.satisfy?(req))

    # Different names don't overlap
    prv = provides('foo', '2', '1', 0, EQ)
    req = requires('bar', '1', '1', 0, EQ | GT)
    assert(!req.satisfy?(prv))
  end

  def provides(name, v, r, e, sense)
    XRPM::Provide.new(name, XRPM::Version.new(v, r, e), sense, nil)
  end

  def requires(name, v, r, e, sense)
    XRPM::Require.new(name, XRPM::Version.new(v, r, e), sense, nil)
  end
end
